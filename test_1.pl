parcours(ihm).
parcours(iam).
parcours(caspar).
parcours(web).
parcours(al).

parcours(iam,plateformes_logicielle_mobile).
parcours(iam,systemes_applications_embarques).
parcours(iam,objets_connectes_services).
parcours(iam,middleware_IOT).

parcours(ihm,conception_evaluation_ihm).
parcours(ihm,technique_interaction_multimodalite).
parcours(ihm,adaptation_interface_environnemetn).
parcours(ihm,interface_tactile).
parcours(ihm,interface_repartie).


parcours(caspar,cryptographie_securite).
parcours(caspar,securite_applications_web).
parcours(caspar,cybersecurite).
parcours(caspar,securite_reseaux).
parcours(caspar,preuve_cryptographie).
parcours(caspar,security_privacy).

parcours(web,conception_application_multimedia).
parcours(web,analyse_indexation_images).
parcours(web,compression).
parcours(web,applications_multimedia).
parcours(web,traitement_avance_images).
parcours(web,techno_donnes_massives).

parcours(al,soa_integration_services).
parcours(al,architectures_logicielles).
parcours(al,architecture_logicielle_cloud).
parcours(al,retro_ingenierie_maintenance_evolution_logiciels).
parcours(al,langages_speciciques_domaines).

/*TODO 
ne pas prendre en compte dans parcours les periodes
	pas besoin?

verifier les duplications de matières
revoir les noms des matieres
	done
*/

/*

	BIN : parcours/matieres raw

IAM 

cours(1,mar,am,plateformes_logicielle_mobile).
cours(1,mer,am,systemes_applications_embarques).
cours(2,mer,am,systemes_applications_embarques).
cours(1,mar,pm,objets_connectes_services).
cours(2,mar,am,middleware_IOT).

IHM
cours(1,lun,am,conception_evaluation_ihm).
cours(1,jeu,am,technique_interaction_multimodalite).
cours(1,ven,am,adaptation_interface_environnement).
cours(2,ven,pm,interface_tactile).
cours(2,ven,am,interface_repartie).

CASPAR
cours(1,lun,pm,cryptographie_securite).
cours(1,mer,am,securite_applications_web).
cours(1,ven,pm,cybersecurite).
cours(2,lun,pm,securite_reseaux).
cours(2,mar,am,preuve_cryptographie).
cours(2,ven,pm,security_privacy).

WEB
cours(1,mer,am,conception_application_multimedia).
cours(2,mer,am,analyse_indexation_images).
cours(1,lun,pm,compression).
cours(1,ven,pm,applications_multimedia).
cours(1,jeu,am,traitement_avance_images).
cours(1,lun,am,techno_donnes_massives).
*/

/*
.......................
	Cours
		avec inclusion des creneaux
			periode
			jour 
			demi journee 
			

*/

cours(1,lun,am,algorithmic_approach).
cours(1,lun,am,conception_evaluation_ihm).
cours(1,lun,am,techno_donnes_massives).

cours(1,lun,pm,compression).
cours(1,lun,pm,cryptographie_securite).
cours(1,lun,pm,soa_integration_services).

cours(1,mar,am,formal_models_computation).
cours(1,mar,am,peer_to_peer).
cours(1,mar,am,plateformes_logicielle_mobile).
cours(1,mar,am,web_donnees).

cours(1,mar,pm,administration_reseau).
cours(1,mar,pm,inginerie_connaissances).
cours(1,mar,pm,objets_connectes_services).
cours(1,mar,pm,performance_eval_networks).
cours(1,mar,pm,genericite_meta_programmation).




cours(1,mer,am,application_relationnelles_web).
cours(1,mer,am,SOA_process_metier).
cours(1,mer,am,conception_application_multimedia).
cours(1,mer,am,content_distribution_wireless_networks).
cours(1,mer,am,securite_applications_web).
cours(1,mer,am,systemes_applications_embarques).

cours(1,mer,pm,technique_expression).

cours(1,jeu,am,technique_interaction_multimodalite).
cours(1,jeu,am,traitement_avance_images).

cours(1,jeu,pm,anglais).
cours(1,jeu,pm,management).

cours(1,ven,am,adaptation_interface_environnement).
cours(1,ven,am,architectures_logicielles).
cours(1,ven,am,evolving_internet).

cours(1,ven,pm,applications_multimedia).
cours(1,ven,pm,cybersecurite).
cours(1,ven,pm,architecture_logicielle_cloud).




cours(2,lun,am,conception_evaluation_ihm).
cours(2,lun,am,virtualized_infrastructure_cloud).

cours(2,lun,pm,interpretation_langages).
cours(2,lun,pm,inginerie_3D).
cours(2,lun,pm,programmation_web_server).
cours(2,lun,pm,securite_reseaux).
cours(2,lun,pm,algorithm_telecomm).

cours(2,mar,am,fouille_donnes).
cours(2,mar,am,middleware_IOT).
cours(2,mar,am,preuve_cryptographie).
cours(2,mar,am,realite_virtuelle).
cours(2,mar,am,retro_ingenierie_maintenance_evolution_logiciels).

cours(2,mar,pm,large_scale_systems).
cours(2,mar,pm,objets_connectes_services).
cours(2,mar,pm,web_semantique).

cours(2,mer,am,analyse_indexation_images).
cours(2,mer,am,systemes_applications_embarques).
cours(2,mer,am,langages_speciciques_domaines).

cours(2,mer,pm,distributed_optimization).

cours(2,jeu,am,bit_torrent_to_privacy).
cours(2,jeu,am,programmmable_web).
cours(2,jeu,am,smart_cards).

cours(2,jeu,pm,green_networking).
cours(2,jeu,pm,management2).

cours(2,ven,am,architectures_logicielles).
cours(2,ven,am,gestion_donnes_multimedia).
cours(2,ven,am,interface_repartie).
cours(2,ven,am,internet_measurement).

cours(2,ven,pm,interface_tactile).
cours(2,ven,pm,security_privacy).

/**
 *
	creneau(DEMI,JOUR,Cours).
	fd_domain(DEMI,JOUR,Cours).
	prerequis(PreRequis,Cours)

	isCours(COURS) :- cours(_,COURS).
	(pas utilisé car on utilise hasPeriode)
	cours(X,Y).
	cours(1,ihm)
	
*/


hasPeriode(PERIODE,COURS) :- cours(PERIODE,COURS).
has2Periode(COURS) :- cours(1,COURS),cours(2,COURS).
vaut2Credit(COURS) :- 
	true \= has2Periode(COURS),
	hasPeriode(_,COURS).
vaut4Credit(COURS) :-
	has2Periode(COURS).


/**
 * tester ci dessous le cas avec juste un element
 * comme (H)
 */

vaut24Credit(L) :-
	vautCredit(L,24).

vautCredit([],0).
vautCredit([H|T],N) :-
	vaut2Credit(H),vautCredit(T,N-2).
vautCredit([H|T],N ):-
	vaut4Credit(H),vautCredit(T,N-4).


correctPreRequis([]).
correctPreRequis([H|T]) :-
	preRequis1(H,T),
	hasPreRequis(T).

preRequis1(H,L):-
	true \= prerequis(H,L).
preRequis(H,L) :-
	trouvePreRequis(H,L).

trouvePreRequis(C,[]).
trouvePreRequis(C,[H|T]) :-
	prerequis(H,C);
	trouvePreRequis(C,T).


/**
 * test avec findall

 * ! pb de variables
 */
trouvePreRequis(C)
 findall(X,prerequis(X,C),RESULTAT),
 verifPreRequis(RESULTAT,L).

verifPreRequis([],_).
verifPreRequis([H|T],L) :-


			/* le code de drive précise les cours co requis

			possibleCours(prerequis,cours)
			possibleCours(cours_periode_1_REQUIS,cours_avec_pre_requis)

			ET
		possibleCours(coursCoRequis_periode_X,coursAvecRequis_periode_X)			possibleCours(coursCoRequis_periode_1,coursAvecRequis_periode_1)
		possibleCours(coursCoRequis_periode_2,coursAvecRequis_periode_2)


			*/
principal(L) :-
hasPreRequis(L),
vaut24Credits(L),
PasChevauchement(L),