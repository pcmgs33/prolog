/**
 *
	creneau(DEMI,JOUR,Cours).
	fd_domain(DEMI,JOUR,Cours).
	prerequis(PreRequis,Cours)

	isCours(COURS) :- cours(_,COURS).
	(pas utilisé car on utilise hasPeriode)
	cours(X,Y).
	cours(1,ihm)
	
*/


hasPeriode(PERIODE,COURS) :- cours(PERIODE,COURS).
has2Periode(COURS) :- cours(1,COURS),cours(2,COURS).
vaut2Credit(COURS) :- 
	true \= has2Periode(COURS),
	hasPeriode(_,COURS).
vaut4Credit(COURS) :-
	has2Periode(COURS).


/**
 * tester ci dessous le cas avec juste un element
 * comme (H)
 */

vaut24Credit(L) :-
	vautCredit(L,24).

vautCredit([],0).
vautCredit([H|T],N) :-
	vaut2Credit(H),vautCredit(T,N-2).
vautCredit([H|T],N ):-
	vaut4Credit(H),vautCredit(T,N-4).


correctPreRequis([]).
correctPreRequis([H|T]) :-
	preRequis1(H,T),
	hasPreRequis(T).

preRequis1(H,L):-
	true \= prerequis(H,L).
preRequis(H,L) :-
	trouvePreRequis(H,L).

trouvePreRequis(_,[]).
trouvePreRequis(C,[H|T]) :-
	prerequis(H,C);
	trouvePreRequis(C,T).


/**
 * test avec findall

 * ! pb de variables
 */
trouvePreRequis(C,L):-
 findall(X,prerequis(X,C),RESULTAT),
 verifPreRequis(RESULTAT,L).

verifPreRequis([],_).

/*

verifPreRequis([H|T],L) :-
*/


/* le code de drive précise les cours co requis

possibleCours(prerequis,cours)
possibleCours(cours_periode_1_REQUIS,cours_avec_pre_requis)

ET
possibleCours(coursCoRequis_periode_X,coursAvecRequis_periode_X)			possibleCours(coursCoRequis_periode_1,coursAvecRequis_periode_1)
possibleCours(coursCoRequis_periode_2,coursAvecRequis_periode_2)


*/

/*
					
	principal(L) :-
	hasPreRequis(L),
	vaut24Credits(L),
	PasChevauchement(L).
*/