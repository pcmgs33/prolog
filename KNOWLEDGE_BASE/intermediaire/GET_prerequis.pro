

requis(1,1,systemes_applications_embarques,objets_connectes_services).

requis(2,1,web_donnees,web_semantique).
requis(2,1,cryptographie_securite,preuve_cryptographie).
requis(2,1,cryptographie_securite,securite_reseaux).

requis(2,2,programmable_web_server,programmable_web_client).
requis(2,2,programmable_web_client,programmable_web_server).

requis(2,1,conception_evaluation_ihm,interface_tactile).
requis(2,1,conception_evaluation_ihm,interface_multisupport).


/*
	La fonction prerequis
	teste une liste de cours pour vérifier les prérequis

	fait, pour chaque cours :
	- cherche les prérequis des cours
	- vérifie si les prérequis sont bien dans la liste des cours

*/

/*
Pour les tests

[web_semantique,programmable_web_client,web_donnees]

*/

/*
	testPrerequis est la fonction qui commence le test des prérequis
	elle appelle prerequis avec une liste vide, qui contiendra les éléments traversés
	ce qui permet de pallier le fait d'avoir une liste ou les cours prérequis ne sont pas
	toujours après les cours qui en ont besoin
*/
testPrerequis([]).
testPrerequis(L) :- prerequis(L,[]).

prerequis([],_).
prerequis([H|T],L) :-
	getPrerequis(H,LISTE_PRE_REQUIS),
	append(T,L,TOT),
	verifyPrerequis(LISTE_PRE_REQUIS,TOT),
	prerequis(T,[H|L]).

verifyPrerequis([],_).
verifyPrerequis([H|T],TOTAL) :-
	member(H,TOTAL),
	verifyPrerequis(T,TOTAL).

getPrerequis(C,RESULTAT) :-
 findall(X,requis(_,_,X,C),RESULTAT).


/*
	la fonction getPrerequis prend tous les prérequis d'un cours
	et renvoit une liste vide si il n'y en a pas
*/

/*
	la fonction verifyPrerequis prend tous les prérequis d'un cours
	et teste leur appartenance a la liste des cours choisis
*/


/*

PREREQUIS AVEC UN CO REQUIS
dans la méthode "classique" on aurait itéré sur la liste et vérifié un a un les requis
cela ne marche pas dans le cas d'un co requis, car les deux sont dépendant
ainsi lorsque l'on a vérifié une partie d'un co requis, on ne le garde plus dans la liste
donc le deuxieme co requis échoue
C'est le même cas lorsque les pré requis sont situés en amont des cours demandant ces pré requis,
on passe sur le pré-requis sans le prendre en compte

plusieurs solutions
tester explicitement les co requis

ou bien
itérer sur le numéro de la liste, aller de 1 a n
	(donc trouver n? ou bien s'arreter sur la liste vide?VIDE !)
ou bien
réécrire la liste dans une variable supplémentaire qui servira a pallier ce probleme

*/

