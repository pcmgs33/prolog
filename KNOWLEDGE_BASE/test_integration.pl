/*||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


														FAITS


||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
*/


/*..........................................................................................
														COURS
..........................................................................................
*/

/*
	les créneaux auraient pu être de la forme
	créneau(1,lun,am,cours(nomcours))

	cela n'aurait rien changé à part
	en + :
	- plus grande modularité
	- plus grande compréhensibilité

	en - :
	- plus mauvaise lisibilité du code
	- plus de difficulté à debugger
	- plus d'appels, donc plus de calculs

	vu que l'on ne fait que des cours,
	le concept de créneau est assez explicite


*/
creneau(1,lun,am,'algorithmic_approach').
creneau(1,lun,am,'conception_evaluation_ihm').
creneau(1,lun,am,'techno_donnees_massives').

creneau(1,lun,pm,'compression').
creneau(1,lun,pm,'cryptographie_securite').
creneau(1,lun,pm,'soa_integration_services').

creneau(1,mar,am,'formal_models_computation').
creneau(1,mar,am,'peer_to_peer').
creneau(1,mar,am,'plateformes_logicielle_mobile').
creneau(1,mar,am,'web_donnees').

creneau(1,mar,pm,'administration_reseau').
creneau(1,mar,pm,'ingenierie_connaissances').
creneau(1,mar,pm,'objets_connectes_services').
creneau(1,mar,pm,'performance_eval_networks').
creneau(1,mar,pm,'genericite_meta_programmation').

creneau(1,mer,am,'application_relationnelles_web').
creneau(1,mer,am,'SOA_process_metier').
creneau(1,mer,am,'conception_application_multimedia').
creneau(1,mer,am,'content_distribution_wireless_networks').
creneau(1,mer,am,'securite_applications_web').
creneau(1,mer,am,'systemes_applications_embarques').

creneau(1,mer,pm,'technique_expression').

creneau(1,jeu,am,'technique_interaction_multimodalite').
creneau(1,jeu,am,'traitement_avance_images').

creneau(1,jeu,pm,'anglais').
creneau(1,jeu,pm,'management').

creneau(1,ven,am,'adaptation_interface_environnement').
creneau(1,ven,am,'architectures_logicielles').
creneau(1,ven,am,'evolving_internet').

creneau(1,ven,pm,'applications_multimedia').
creneau(1,ven,pm,'cybersecurite').
creneau(1,ven,pm,'architecture_logicielle_cloud').

creneau(2,lun,am,'conception_evaluation_ihm').
creneau(2,lun,am,'virtualized_infrastructure_cloud').

creneau(2,lun,pm,'interpretation_langages').
creneau(2,lun,pm,'ingenierie_3D').
creneau(2,lun,pm,'programmable_web_server').
creneau(2,lun,pm,'securite_reseaux').
creneau(2,lun,pm,'algorithm_telecomm').

creneau(2,mar,am,'fouille_donnees').
creneau(2,mar,am,'middleware_IOT').
creneau(2,mar,am,'preuve_cryptographie').
creneau(2,mar,am,'realite_virtuelle').
creneau(2,mar,am,'retro_ingenierie_maintenance_evolution_logiciels').

creneau(2,mar,pm,'large_scale_systems').
creneau(2,mar,pm,'objets_connectes_services').
creneau(2,mar,pm,'web_semantique').

creneau(2,mer,am,'analyse_indexation_images').
creneau(2,mer,am,'systemes_applications_embarques').
creneau(2,mer,am,'langages_speciciques_domaines').

creneau(2,mer,pm,'distributed_optimization').

creneau(2,jeu,am,'bit_torrent_to_privacy').
creneau(2,jeu,am,'programmable_web_client').
creneau(2,jeu,am,'smart_cards').

creneau(2,jeu,pm,'green_networking').
creneau(2,jeu,pm,'management2').

creneau(2,ven,am,'architectures_logicielles').
creneau(2,ven,am,'gestion_donnees_multimedia').
creneau(2,ven,am,'interface_multisupport').
creneau(2,ven,am,'internet_measurement').

creneau(2,ven,pm,'interface_tactile').
creneau(2,ven,pm,'security_privacy').

/*..........................................................................................
														ETUDIANTS
..........................................................................................
*/
/*
	de la forme etudiant('FirstName','LastName','Major')
*/

etudiant('Audric','Chabert','IHM').
etudiant('MacKensie','Jackson','CASPAR').
etudiant('Cameran','Acevedo','IAM').
etudiant('Amela','Crane','IAM').
etudiant('Merritt','Ramsey','WEB').
etudiant('Dale','Cannon','IAM').
etudiant('Alexander','Ferguson','CASPAR').
etudiant('Serina','Cunningham','AL').
etudiant('Britanni','Beard','WEB').
etudiant('Sopoline','Alvarado','CASPAR').
etudiant('Madeson','Cook','AL').
etudiant('Kellie','Harrell','WEB').
etudiant('Quamar','Michael','IAM').
etudiant('Ria','Figueroa','IAM').
etudiant('September','Espinoza','WEB').
etudiant('Amena','Koch','IHM').
etudiant('Herrod','Reynolds','IHM').
etudiant('Rahim','Daniel','WEB').
etudiant('Chelsea','Merrill','WEB').
etudiant('Shaine','Marshall','IAM').
etudiant('Samson','Morrow','IAM').
etudiant('Camden','Long','CASPAR').
etudiant('Brynn','Mccray','AL').
etudiant('Julie','Stark','WEB').
etudiant('Gloria','White','WEB').
etudiant('Keiko','Castaneda','IAM').
etudiant('Howard','Mercado','AL').
etudiant('Shelby','Benson','AL').
etudiant('Angela','York','IHM').
etudiant('Constance','Orr','CASPAR').
etudiant('Derek','Salinas','AL').
etudiant('Sylvia','Velez','WEB').
etudiant('Lesley','Slater','IHM').
etudiant('Kirsten','George','WEB').
etudiant('Taylor','Mccoy','IAM').
etudiant('Rhona','Whitaker','WEB').
etudiant('Yoko','Holman','AL').
etudiant('Jordan','Bowers','CASPAR').
etudiant('Colette','Guy','IHM').
etudiant('Rebekah','Hinton','IHM').
etudiant('Walker','Knowles','IAM').
etudiant('Martena','Mckinney','CASPAR').
etudiant('Kylee','Alston','CASPAR').
etudiant('Nelle','Mcclain','AL').
etudiant('Destiny','Potter','AL').
etudiant('Owen','French','WEB').
etudiant('Angelica','Bender','IAM').
etudiant('Kyla','Warren','IAM').
etudiant('Vaughan','Mercado','IHM').
etudiant('Melissa','Strickland','IAM').
etudiant('Maggy','Cohen','CASPAR').
etudiant('Latifah','Bradshaw','CASPAR').
etudiant('Lara','Velasquez','IHM').
etudiant('Janna','Gonzales','AL').
etudiant('Harding','Burns','CASPAR').
etudiant('Stella','Gill','WEB').
etudiant('Winter','Wilder','AL').
etudiant('Illana','Benjamin','IHM').
etudiant('Joseph','Mckee','CASPAR').
etudiant('Zorita','Horton','IHM').
etudiant('Steven','Hall','IAM').
etudiant('Evelyn','Edwards','AL').
etudiant('Rhiannon','Mueller','IHM').
etudiant('Hiroko','David','IAM').
etudiant('Brenda','Clarke','AL').
etudiant('Bevis','Hopper','AL').
etudiant('Kiara','Tanner','WEB').
etudiant('Jenna','Taylor','WEB').
etudiant('Oren','Delacruz','IAM').
etudiant('Cade','Barrera','CASPAR').
etudiant('Laura','Montgomery','AL').
etudiant('Kirestin','Lucas','WEB').
etudiant('Ferris','Hickman','IAM').
etudiant('Erin','Wise','IAM').
etudiant('Grant','Golden','AL').
etudiant('Gwendolyn','Lambert','IHM').
etudiant('Scarlet','Hinton','IAM').
etudiant('Kellie','Weber','AL').
etudiant('Lester','Roy','AL').
etudiant('Cameran','Diaz','AL').
etudiant('Fatima','Rios','IHM').
etudiant('Hannah','Baxter','IHM').
etudiant('Julian','Gilmore','AL').
etudiant('Cooper','Perez','CASPAR').
etudiant('Maite','Spencer','CASPAR').
etudiant('Shaine','Galloway','CASPAR').
etudiant('Philip','Shepard','CASPAR').
etudiant('Faith','Ferrell','AL').
etudiant('Amena','Lopez','IAM').
etudiant('Keefe','Wade','WEB').
etudiant('Daquan','Foley','WEB').
etudiant('Dalton','Perez','AL').
etudiant('Simone','Hicks','IHM').
etudiant('Perry','Schneider','IHM').
etudiant('Isadora','Farrell','IHM').
etudiant('Iliana','Tucker','IAM').
etudiant('Debra','Mckenzie','IAM').
etudiant('Ian','Spence','IAM').
etudiant('Kaden','Holder','IHM').
etudiant('Nevada','Bentley','AL').
etudiant('Fletcher','Harrell','IAM').


/*..........................................................................................
														PARCOURS
..........................................................................................
*/

parcours('IHM').
parcours('IAM').
parcours('CASPAR').
parcours('WEB').
parcours('AL').

parcours('IAM',plateformes_logicielle_mobile).
parcours('IAM',systemes_applications_embarques).
parcours('IAM',objets_connectes_services).
parcours('IAM',middleware_IOT).

parcours('IHM',conception_evaluation_ihm).
parcours('IHM',technique_interaction_multimodalite).
parcours('IHM',adaptation_interface_environnement).
parcours('IHM',interface_tactile).
parcours('IHM',interface_multisupport).


parcours('CASPAR',cryptographie_securite).
parcours('CASPAR',securite_applications_web).
parcours('CASPAR',cybersecurite).
parcours('CASPAR',securite_reseaux).
parcours('CASPAR',preuve_cryptographie).
parcours('CASPAR',security_privacy).

parcours('WEB',conception_application_multimedia).
parcours('WEB',analyse_indexation_images).
parcours('WEB',compression).
parcours('WEB',applications_multimedia).
parcours('WEB',traitement_avance_images).
parcours('WEB',techno_donnees_massives).

parcours('AL',soa_integration_services).
parcours('AL',soa_integration_services).
parcours('AL',architectures_logicielles).
parcours('AL',architecture_logicielle_cloud).
parcours('AL',retro_ingenierie_maintenance_evolution_logiciels).
parcours('AL',langages_speciciques_domaines).


/*..........................................................................................
														REQUIS
..........................................................................................
*/

/*
le cours requis est placé avant le cours concerné


.......................................
TODO
faire les cas sans et avec listes
..........................................

requis(1,cours,cours).
requis(2,cours,cours).
moins bien car
- redondance "inutile"

requis([1,2],cours,cours).
mieux! car
- plus lisible
- 

AU FINAL cette derniere forme est peu utile car
le cas ou un cours est requis sur deux périodes n'apparaît pas ici
D'ailleurs il faut mettre deux fois les périodes, car les cours ont chacun leur période
et un cours sur une période(ou deux) peut avoir un cours pré/co-requis sur une période
(ou deux)



forme:
prerequis()
corequis()

conjointement()
a ne pas utiliser car trop lourde, pénible et source d'erreur
a l'avantage de simplifier le cas programmable_webCLIENT/SERVER

trouver si possible un script qui échange le dernier mot pour le mettre devant


Note :
la disposition prise pour requis(1,2,3,4)
fait que x1 >= x2
en effet on ne peut pas dire qu'un cours en période 1 doit etre pris avant de faire
le cours requis en periode 2

expliquer que le premier est le cours dépendant
donc le cours dépendant ne peut dépendre que de quelqun avant ou au meme moment que lui
, et pas après

*/

requis(1,1,systemes_applications_embarques,objets_connectes_services).

requis(2,1,web_donnees,web_semantique).
requis(2,1,cryptographie_securite,preuve_cryptographie).
requis(2,1,cryptographie_securite,securite_reseaux).

requis(2,2,programmable_web_server,programmable_web_client).
requis(2,2,programmable_web_client,programmable_web_server).

requis(2,1,conception_evaluation_ihm,interface_tactile).
requis(2,1,conception_evaluation_ihm,interface_multisupport).

/*
	faits de tests
	les choix permettent de tester le nombre d'etudiants inscrits a un cours en particulier
	dans ce cas il ne faut pas juste compter le nombre d'etudiants qui suivent un cours d'un parcours
	
	mais aussi les autres cours

	choix('MacKensie','Jackson',[conception_evaluation_ihm,technique_interaction_multimodalite,adaptation_interface_environnement,analyse_indexation_images,interface_multisupport]).
	choix('Audric','Chabert',[anglais,cybersecurite,technique_expression,large_scale_systems,distributed_optimization,fouille_donnees]).

	ils sont utilisés pour tester statiquement les prédicats principal(Nom,Prenom)

	si ces choix sont dans la base de connaissances, alors les fonctions assertChoix ne fonctionneront pas
		car on ne peut rajouter de regle que si il n'existe aucune regles de ce type dans la base 

*/

	choix('MacKensie','Jackson',[conception_evaluation_ihm,technique_interaction_multimodalite,adaptation_interface_environnement,analyse_indexation_images,interface_multisupport]).
	choix('Audric','Chabert',[anglais,cybersecurite,technique_expression,large_scale_systems,distributed_optimization,fouille_donnees]).


/*||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


														REGLES
														

||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
*/



/*..........................................................................................
														CREDITS
..........................................................................................
*/

/**
 *
	creneau(DEMI,JOUR,Cours).
	fd_domain(DEMI,JOUR,Cours).
	prerequis(PreRequis,Cours)

	isCours(COURS) :- cours(_,COURS).
	(pas utilisé car on utilise hasPeriode)
	cours(X,Y).
	cours(1,ihm)
	
*/

/*
	predicat : hasPeriode
	But : offrire une notation simple pour récupérer
			la période d'un cours

	Note : on aurait pu écrire un predicat periode(1,cours)
			mais il aurait fallut changer son utilisation dans les predicats


*/
hasPeriode(PERIODE,COURS) :- creneau(PERIODE,_,_,COURS).
/* 
VARIANTE
has2Periode(COURS) :- creneau(1,_,_,COURS),creneau(2,_,_,COURS).
*/
has2Periode(COURS) :- hasPeriode(1,COURS),hasPeriode(2,COURS).

has1Periode(COURS) :- hasPeriode(_,COURS), notHas2Periode(COURS).

notHas2Periode(COURS) :- has2Periode(COURS),!,fail;true.


/* 
VARIANTE
vaut2Credit(COURS) :- 
	true \= has2Periode(COURS),
	hasPeriode(_,COURS).	
		^
		| ne marche pas dans le cas 4Credits
*/
/*
	cette predicat teste qu'on au JUSTE 2 credits
	par cours
	il teste que l'on est bien sur 1 période
	donc que l'on n'est pas sur 2 périodes

	ensuite on teste quand même que l'on est au
	moins sur une période
	(pour les cas ou le cours n'existe pas du tout)

	les cas sont disjoints, donc
	le cas 2 période est disjoint de 1 periode
	le cas 1 periode est la négation de 2 période ET au moins une période
*/
vaut2Credit(COURS) :-
	has1Periode(COURS).

vaut4Credit(COURS) :-
	has2Periode(COURS).


/*
 * tester ci dessous le cas avec juste un element
 * comme (H)

 	le predicat ne marche pas si il n'y a qu'un seul
 	cours qui vaut 12 crédits
 	a fixer
 	NON a ne pas fixer, car on prend bien une liste
 */

/*
	On pourrait faire une variante avec
	credit(N,COURS)
	pour faire

vautCredit([H|T],NTOT) :-
	vautCredit(H,NC), N1 is NTOT - NC, vautCredit(T,N1).

	Quelle est la différence entre le stockage sur plusieurs faits
	ou bien dans une grande liste

	faits
		- lisibilité
		- "généricité" tant que l'on change bien les noms des faits
	liste
		- simplicité

*/

vaut24Credit(L) :-
	calculCredit(L,N),!,
	N is 24.

vaut12Credit(L) :-
	calculCredit(L,N),!,
	N is 12.

/*
ATTENTION

remplacement des deux dernieres regles de vautCredit
*/

calculCredit(L,N) :- vautCredit(L,0,N).


vautCredit([],N,N).
vautCredit([],INIT,_) :- vautCredit([],INIT,INIT).

vautCredit([H|T],INIT,N):-
	vaut4Credit(H),N1 is INIT+4, vautCredit(T,N1,N).
vautCredit([H|T],INIT,N) :-
	vaut2Credit(H),N1 is INIT+2, vautCredit(T,N1,N).



/*..........................................................................................
														REQUIS
..........................................................................................
*/





/*
	testPrerequis est le predicat qui commence le test des prérequis
	elle appelle prerequis avec une liste vide, qui contiendra les éléments traversés
	ce qui permet de pallier le fait d'avoir une liste ou les cours prérequis ne sont pas
	toujours après les cours qui en ont besoin
*/
/*
Pour les tests

QUI RENVOIENT NO
testPrerequis([web_semantique,programmable_web_client,web_donnees]).
testPrerequis([web_semantique,programmable_web_client,programmable_web_server]).

QUI RENVOIENT YES
Basique :
testPrerequis([web_semantique,programmable_web_client,web_donnees,programmable_web_server]).

Complet:
testPrerequis([systemes_applications_embarques,objets_connectes_services,web_donnees,web_semantique,cryptographie_securite,preuve_cryptographie,cryptographie_securite,securite_reseaux,programmable_web_server,programmable_web_client,programmable_web_client,programmable_web_server,interface_tactile,conception_evaluation_ihm,interface_multisupport]).

En cas réel (avec des cours sans pré-requis dans la liste):

testPrerequis([analyse_indexation_images,systemes_applications_embarques,objets_connectes_services,web_donnees,web_semantique,cryptographie_securite,preuve_cryptographie]).
testPrerequis([analyse_indexation_images,cryptographie_securite,securite_reseaux,programmable_web_server,programmable_web_client,programmable_web_client,programmable_web_server]).
testPrerequis([analyse_indexation_images,conception_evaluation_ihm,interface_tactile,interface_multisupport]).

*/
testPrerequis([]).
testPrerequis(L) :- prerequis(L,[]).

/*

*/
prerequis([],_).
prerequis([H|T],L) :-
	getPrerequis(H,LISTE_PRE_REQUIS),
	append(T,L,TOT),
	verifyPrerequis(LISTE_PRE_REQUIS,TOT),
	prerequis(T,[H|L]).

/*
	le predicat verifyPrerequis prend tous les prérequis d'un cours
	et teste leur appartenance a la liste des cours choisis
*/
verifyPrerequis([],_).
verifyPrerequis([H|T],TOTAL) :-
	member(H,TOTAL),
	verifyPrerequis(T,TOTAL);
	display('erreur_pre_requis on ne trouve pas le cours requis suivant : '),
	display(H),
	fail.

/*
	le predicat getPrerequis prend tous les prérequis d'un cours
	et renvoit une liste vide si il n'y en a pas
*/
/*
	tester avec
		getPrerequis(interface_tactile,L).
			renvoit [conception_evaluation_ihm]

		getPrerequis(conception_evaluation_ihm,L).
			renvoit []
*/
getPrerequis(C,RESULTAT) :-
 findall(X,requis(_,_,X,C),RESULTAT).


/*
NOTE : sur le predicat prerequis

PREREQUIS AVEC UN CO REQUIS
dans la méthode "classique" on aurait itéré sur la liste et vérifié un a un les requis
cela ne marche pas dans le cas d'un co requis, car les deux sont dépendant
ainsi lorsque l'on a vérifié une partie d'un co requis, on ne le garde plus dans la liste
donc le deuxieme co requis échoue
C'est le même cas lorsque les pré requis sont situés en amont des cours demandant ces pré requis,
on passe sur le pré-requis sans le prendre en compte

plusieurs solutions
tester explicitement les co requis

ou bien
itérer sur le numéro de la liste, aller de 1 a n
	(donc trouver n? ou bien s'arreter sur la liste vide? ici on s'arrête sur liste vide !)
ou bien
réécrire la liste dans une variable supplémentaire qui servira a pallier ce probleme
Ce qui est fait dans le predicat prerequis, on rappelle le predicat avec dans la seconde liste
les éléments qu'on a déja testés, éléments qui seront rajoutés à la liste des tests avec un append
*/


/*..........................................................................................
														FEATURES
..........................................................................................
*/


/* RECUPERER LA LISTE DES COURS D'UN PARCOURS */
/*
	Tester avec getParcours(P,L).
		ou bien
				getParcours('IHM',L).
*/
getParcours(P,L) :- setof(Z, parcours(P,Z), L).


/* TROUVER LE NOMBRE D'ETUDIANTS INSCRITS A UN PARCOURS */
/*
	Tester avec getNbInscrits(P,N).
*/
getNbInscrits(PARCOURS,RESULTAT) :-
 setof(X^Y,etudiant(X,Y,PARCOURS),L),
 length(L,RESULTAT).

/* TROUVER LE NOMBRE D'ETUDIANTS INSCRITS A UN COURS PARTICULIER */
/*
	tester avec 
		getNbInscritsCours('conception_evaluation_ihm',N).
			rend 20 car il y a les 19 etudiants du parcours IHM ainsi que la personne qui le suit en option
		getNbInscritsCours('anglais',N).
			rend 1
*/
getNbInscritsCours(COURS,RES) :-
	parcours(X,COURS),
	getNbInscrits(X,N),
	compteNbInscritCours(COURS,M),
	RES is N+M.

getNbInscritsCours(COURS,M) :-
	compteNbInscritCours(COURS,M).

compteNbInscritCours(COURS,N) :-
	findall(L,choix(_,_,L),LISTE_CHOIX),
	compteListeChoix(COURS,LISTE_CHOIX,N).

compteListeChoix(_,[],0).
compteListeChoix(E,[H|T],N) :-
	member(E,H),
	compteListeChoix(E,T,N1),
	N is N1+1;
	compteListeChoix(E,T,N).


/* VERIFIER QU'UN PARCOURS REPONDE BIEN A LA REGLE DES 12 CREDITS */
/* EXEMPLES 
	verifieParcours('IHM').
		renvoit true
	verifieParcours('IHMs').
		sors en erreur, IHMs n'existant pas
*/
verifieParcours(P) :-
	getParcours(P,L),
	vaut12Credit(L),
	verifChevauchement(L,[]).

/*
	changer de place un cours vis a vis des etudiants qui le suivent
*/



/*..........................................................................................
														VERIFICATION PRINCIPALE
	prédicats qui servent au prédicat principal, celui qui sers à valider un choix de cours
..........................................................................................
*/



/*
	VerifChevauchement

	remarque : le predicat s'appelle avec une liste vide, utilisée pour le traitement. On aurait pu utiliser une predicat intermédiaire pour l'appel, 
	mais cela rajoute une predicat supplémentaire qui n'augmente pas la lisibilité du code selon moi.

	Le predicat procède comme suit :
	- prend un cours
	- vérifie qu'il ne chevauche pas tous les autres cours
	- range le cours vérifié dans la liste des cours qui peuvent se chevaucher
	- se rappelle tant que la liste n'a pas été totalement vérifiée (cas terminal sur liste vide)

	note : on prend soin de rajouter les cours que l'on a vérifié a la liste de tous les cours pouvant se chevaucher,
		   car même si un cours ne chevauche pas tous les autres cours, certains cours peuvent se chevaucher entre eux,
		   d'où la nécéssité de conserver tous les cours pour la vérification

 */
/*
tests verifChevauchement

yes :
	verifChevauchement([algorithmic_approach,compression],[]).
	verifChevauchement([algorithmic_approach,soa_integration_services,ingenierie_connaissances,technique_expression],[]).
	
no :
	verifChevauchement([algorithmic_approach,conception_evaluation_ihm],[]).
	verifChevauchement([algorithmic_approach,soa_integration_services,ingenierie_connaissances,technique_expression,conception_evaluation_ihm],[]).

*/
verifChevauchement([],_).
verifChevauchement([H|T],V) :-
	append(T,V,TOT),
    verifSimple(H,TOT),
    verifChevauchement(T,[H|V]).

/*
verifSimple
    verifie qu'un cours ne chevauche pas avec un des cours de la liste
on utilise le cut pour simplifier le processus, faire du backtracking ici est coûteux en temps
et n'apporte pas de bénéfices
*/
verifSimple(_,[]).

verifSimple(X,[H|T]) :-
    verifTresSimple(X,H),!,
    verifSimple(X,T).
    
/*
 verifTresSimple
 verifie que deux cours ne se chevauchent pas
 
pour ce faire il vérifie :
 que les cours ne soient pas sur la meme période 				( si c'est le cas alors il n'y a pas de chevauchement du tout)
 OU BIEN que les cours ne soient pas sur le meme jour 			(idem)
 OU BIEN que les cours ne soient pas sur la même demi-journée	(idem)

 */
verifTresSimple(X,Y) :-
	verifPeriode(X,Y);
	verifJour(X,Y);
	verifDemijour(X,Y).

/*
	On récupère ici la liste des périodes, qui peut donc être [1], [2] ou [1,2]
	on vérifie ensuite que les périodes soient bien distinctes
*/
/*
test : verifPeriode
success: 	[1],[2]
			[2],[1]
fail: 		[1],[1,2]
			...
*/
verifPeriode(X,Y) :-
    setof(P,creneau(P,_,_,X),P1),
    setof(P,creneau(P,_,_,Y),P2),
    memberPeriode(P1,P2).

/*
	On récupère les jours des deux matières, si elles sont différentes alors les cours ne se chevauchent pas
*/
verifJour(X,Y) :-
    creneau(_,J1,_,X),
    creneau(_,J2,_,Y),
    J1 \= J2.

/*
	On récupère les demi-journées des deux matières, si elles sont différentes alors les cours ne se chevauchent pas
*/
verifDemijour(X,Y) :-
    creneau(_,_,DJ1,X),
    creneau(_,_,DJ2,Y),
    DJ1 \= DJ2.

/*
	Vérifie que les périodes des deux cours soient bien distinctes
	Si les cours sont sur au moins une période en commun, alors le test échoue
	sinon les cours sont bien sur deux périodes distinctes
*/
memberPeriode([],[]).
memberPeriode(P1,P2) :-
	member(1,P1),member(1,P2),!,fail;
	member(2,P1),member(2,P2),!,fail;
	true.









/*
	***********************************************************************************
	PREDICAT DE VERIFICATION PRINCIPALES
	le point d'entrée pour la vérification d'un choix de parcours
	***********************************************************************************

	il y a deux versions de cette vérification, 
	une ou l'on rentre directement la liste des cours optionnels
		principal(PRENOM,NOM,L)

	et une autre ou cette liste est déja en base (car dans le fichier de données OU bien rentrée avec le prédicat asserta() )
		principal(PRENOM,NOM)
		dans ce cas les informations sont stockées dans des faits de la forme choix(Prenom,Nom,ListeDeCoursOptionnels)
*/
principal(PRENOM,NOM) :-
	etudiant(PRENOM,NOM,PARCOURS),
	choix(PRENOM,NOM,L),
	getParcours(PARCOURS,PREDEF),
	append(L,PREDEF,TOTAL),
	testPrerequis(TOTAL),
	vaut24Credit(TOTAL),
	verifChevauchement(TOTAL,[]).


principal(PRENOM,NOM,L) :-
	etudiant(PRENOM,NOM,PARCOURS),
	getParcours(PARCOURS,PREDEF),
	append(L,PREDEF,TOTAL),
	testPrerequis(TOTAL),
	vaut24Credit(TOTAL),
	verifChevauchement(TOTAL,[]).





/*
	TESTS DU PREDICAT PRINCIPAL

	(utiliser "trace." pour voir le fonctionnement)

yes :
	principal('Audric','Chabert').
	principal('Audric','Chabert').

	principal('MacKensie','Jackson',[conception_evaluation_ihm,technique_interaction_multimodalite,adaptation_interface_environnement,analyse_indexation_images,interface_multisupport]).

no :
	principal('Audric','Chabert',[conception_evaluation_ihm,technique_interaction_multimodalite,adaptation_interface_environnement,analyse_indexation_images]).
		il manque des cours optionnels

	principal('Audric','Chabert',[web_semantique,technique_interaction_multimodalite,adaptation_interface_environnement,analyse_indexation_images]).
		le cours web_semantique requiert le cours web_donnees
		REMARQUE : 	ce cas affiche une exception visible dans la console prolog
					on verra :
						erreur_pre_requis on ne trouve pas le cours requis suivant : web_donnees
					c'est un apercu d'un traitement en erreur qui ressort textuellement la cause et l'emplacement de l'erreur
					cela peut être utile pour la coordinatrice de 5ème année
					cet exemple peut être généralisé
					il est localisé dans le predicat verifyPrerequis

		principal('Audric','Chabert',[web_semantique,web_donnees,conception_evaluation_ihm,anglais,analyse_indexation_images]).
			conception_evaluation_ihm est en double, il est déjà présent dans le parcours
*/


/* 
	principal(all)
	verifie tous les parcours
		- recupère tous les parcours
		- itère sur chaque parcours
			- vérifie le parcours
	puis vérifie tous les choix entrés
		- récupère le nom/prénom des étudiants ayant un choix entré dans la base
		- lance la fonction principal qui va elle même retrouver les choix entrés
			et les testes

	ce predicat se teste tel quel, 
		principal(all).

	c'est une implémentation simpliste de la fonctionnalité demandée :
	peut-on modifier le créneau de place sans que cela ne devienne inconsistent?
	il faut modifier un créneau dans la base, puis lancer principal(all)
*/

principal(all):-
	setof(Z,parcours(Z),LP),
	parcours_iter(LP),
	findall([X|Y],choix(X,Y,_),LC),
	choix_iter(LC).

parcours_iter([]).
parcours_iter([H|T]) :-
	verifieParcours(H),
	parcours_iter(T).

choix_iter([]).
choix_iter([[X|Y]|T]):-
	principal(X,Y),
	choix_iter(T).


/*

Voir la liste des cours qu'un étudiant peut suivre lorsqu'il a déja choisi sa spé

il faut que :
- que le cours n'est pas dans les cours obligatoires,
- que le cours n'est pas en chevauchement avec un des cours obligatoires
- que les co requis, le cas échéant, ne soient pas en chevauchement 

et indiquer les co-requis et pré requis
donc les mettre ensemble
(peut etre display)
*/


/*
	liste tous les cours qui ne chevauchent pas les cours obligatoires d'un etudiant

	- trouve tous les créneaux
	- trouve tous les cours obligatoires
	- itère dessus
*/
/*
	tester avec 
		liste_options_possibles('Audric','Chabert',L).
*/
liste_options_possibles(Nom,Prenom,L) :-
	findall(X,creneau(_,_,_,X),COURS),
	etudiant(Nom,Prenom,Filiere),
	setof(X,parcours(Filiere,X), OBLIG),
	itere(OBLIG,COURS,L).

/*
	détermine tous les cours qui ne chevauchent pas les cours obligatoires

	- vérifie qu'un cours n'est pas déjà dans les cours obligatoires
	- vérifie qu'il ne chevauche pas un des cours obligatoires
	- récupère tous les cours pré requis/ co requis du cours à tester
	- itère sur ces cours
*/
itere(_,[],[]).
itere(OBLIG,[H_COURS|T_COURS],[[H_COURS,REQUIS]|RES]) :-
	isNotMember(H_COURS,OBLIG),
	verifChevauchement([H_COURS|OBLIG],[]),
	findall(Y,requis(_,_,Y,H_COURS),REQUIS),
	testRequis(REQUIS,OBLIG),
	itere(OBLIG,T_COURS,RES).

itere(OBLIG,[H_COURS|T_COURS],L) :-
	itere(OBLIG,T_COURS,L).

/*
	vérifie que chaque cours :
	- soit est déjà membre
	- soit ne chevauche pas les cours obligatoires
*/
testRequis([],_).
testRequis([H|T],OBLIG) :-
	member(H,OBLIG),
	testRequis(T,OBLIG).

testRequis([H|T],OBLIG) :-
	verifChevauchement(H,OBLIG),
	testRequis(T,OBLIG).

/*
	NB on ne vérifie pas les contraintes ENTRE les cours que l'on peut choisir en option,
	car il faudrait tester TOUS les cas ou ces cours pourraient se chevaucher
*/


isNotMember(L1,L2) :-
	member(L1,L2),!,fail;
	true.

/*

	la fonction assertChoix
	sers à créer un choix de cours optionnels pour un étudiant

	il procède comme ceci :
	- il teste que la liste des choix est bien valide par rapport au parcours choisi
	- il rajoute le fait "choix" qui contiendra les informations des cours optionnels dans la base de connaissance

	ATTENTION !
	si il existe dans la base de connaissance (dans le fichier prolog chargé) des règles de type "choix", 
	alors le fait de rajouter un choix provoquera une exception
	Il faut donc supprimer du fichier prolog tous les prédicats du type choix(A,B,C)
	

	se teste avec

		assertChoix('MacKensie','Jackson',[conception_evaluation_ihm,technique_interaction_multimodalite,adaptation_interface_environnement,analyse_indexation_images,interface_multisupport]).

		retractChoix('MacKensie','Jackson').

*/
assertChoix(Nom,Prenom,L) :-
	principal(Nom,Prenom,L),
	asserta(choix(Nom,Prenom,L)).

retractChoix(Nom,Prenom) :-
	retract(choix(Nom,Prenom,_)).