
/*

	BIN : parcours/matieres raw

IAM 

cours(1,mar,am,plateformes_logicielle_mobile).
cours(1,mer,am,systemes_applications_embarques).
cours(2,mer,am,systemes_applications_embarques).
cours(1,mar,pm,objets_connectes_services).
cours(2,mar,am,middleware_IOT).

IHM
cours(1,lun,am,conception_evaluation_ihm).
cours(1,jeu,am,technique_interaction_multimodalite).
cours(1,ven,am,adaptation_interface_environnement).
cours(2,ven,pm,interface_tactile).
cours(2,ven,am,interface_repartie).

CASPAR
cours(1,lun,pm,cryptographie_securite).
cours(1,mer,am,securite_applications_web).
cours(1,ven,pm,cybersecurite).
cours(2,lun,pm,securite_reseaux).
cours(2,mar,am,preuve_cryptographie).
cours(2,ven,pm,security_privacy).

WEB
cours(1,mer,am,conception_application_multimedia).
cours(2,mer,am,analyse_indexation_images).
cours(1,lun,pm,compression).
cours(1,ven,pm,applications_multimedia).
cours(1,jeu,am,traitement_avance_images).
cours(1,lun,am,techno_donnes_massives).
*/
