parcours(ihm).
parcours(iam).
parcours(caspar).
parcours(web).
parcours(al).

parcours(iam,plateformes_logicielle_mobile).
parcours(iam,systemes_applications_embarques).
parcours(iam,objets_connectes_services).
parcours(iam,middleware_IOT).

parcours(ihm,conception_evaluation_ihm).
parcours(ihm,technique_interaction_multimodalite).
parcours(ihm,adaptation_interface_environnement).
parcours(ihm,interface_tactile).
parcours(ihm,interface_multisupport).


parcours(caspar,cryptographie_securite).
parcours(caspar,securite_applications_web).
parcours(caspar,cybersecurite).
parcours(caspar,securite_reseaux).
parcours(caspar,preuve_cryptographie).
parcours(caspar,security_privacy).

parcours(web,conception_application_multimedia).
parcours(web,analyse_indexation_images).
parcours(web,compression).
parcours(web,applications_multimedia).
parcours(web,traitement_avance_images).
parcours(web,techno_donnes_massives).

parcours(al,soa_integration_services).
parcours(al,soa_integration_services).
parcours(al,architectures_logicielles).
parcours(al,architecture_logicielle_cloud).
parcours(al,retro_ingenierie_maintenance_evolution_logiciels).
parcours(al,langages_speciciques_domaines).


cours(1,lun,am,algorithmic_approach).
cours(1,lun,am,conception_evaluation_ihm).
cours(1,lun,am,techno_donnes_massives).

cours(1,lun,pm,compression).
cours(1,lun,pm,cryptographie_securite).
cours(1,lun,pm,soa_integration_services).

cours(1,mar,am,formal_models_computation).
cours(1,mar,am,peer_to_peer).
cours(1,mar,am,plateformes_logicielle_mobile).
cours(1,mar,am,web_donnees).

cours(1,mar,pm,administration_reseau).
cours(1,mar,pm,inginerie_connaissances).
cours(1,mar,pm,objets_connectes_services).
cours(1,mar,pm,performance_eval_networks).
cours(1,mar,pm,genericite_meta_programmation).

cours(1,mer,am,application_relationnelles_web).
cours(1,mer,am,SOA_process_metier).
cours(1,mer,am,conception_application_multimedia).
cours(1,mer,am,content_distribution_wireless_networks).
cours(1,mer,am,securite_applications_web).
cours(1,mer,am,systemes_applications_embarques).

cours(1,mer,pm,technique_expression).

cours(1,jeu,am,technique_interaction_multimodalite).
cours(1,jeu,am,traitement_avance_images).

cours(1,jeu,pm,anglais).
cours(1,jeu,pm,management).

cours(1,ven,am,adaptation_interface_environnement).
cours(1,ven,am,architectures_logicielles).
cours(1,ven,am,evolving_internet).

cours(1,ven,pm,applications_multimedia).
cours(1,ven,pm,cybersecurite).
cours(1,ven,pm,architecture_logicielle_cloud).




cours(2,lun,am,conception_evaluation_ihm).
cours(2,lun,am,virtualized_infrastructure_cloud).

cours(2,lun,pm,interpretation_langages).
cours(2,lun,pm,inginerie_3D).
cours(2,lun,pm,programmable_web_server).
cours(2,lun,pm,securite_reseaux).
cours(2,lun,pm,algorithm_telecomm).

cours(2,mar,am,fouille_donnes).
cours(2,mar,am,middleware_IOT).
cours(2,mar,am,preuve_cryptographie).
cours(2,mar,am,realite_virtuelle).
cours(2,mar,am,retro_ingenierie_maintenance_evolution_logiciels).

cours(2,mar,pm,large_scale_systems).
cours(2,mar,pm,objets_connectes_services).
cours(2,mar,pm,web_semantique).

cours(2,mer,am,analyse_indexation_images).
cours(2,mer,am,systemes_applications_embarques).
cours(2,mer,am,langages_speciciques_domaines).

cours(2,mer,pm,distributed_optimization).

cours(2,jeu,am,bit_torrent_to_privacy).
cours(2,jeu,am,programmmable_web_client).
cours(2,jeu,am,smart_cards).

cours(2,jeu,pm,green_networking).
cours(2,jeu,pm,management2).

cours(2,ven,am,architectures_logicielles).
cours(2,ven,am,gestion_donnes_multimedia).
cours(2,ven,am,interface_multisupport).
cours(2,ven,am,internet_measurement).

cours(2,ven,pm,interface_tactile).
cours(2,ven,pm,security_privacy).

/*
	Cette fonction renvoit les cours qui sont sur deux periodes 
*/

getCoursDeParcours_Sur2Periodes(L):- parcours(_,L),cours(1,_,_,L),cours(2,_,_,L).