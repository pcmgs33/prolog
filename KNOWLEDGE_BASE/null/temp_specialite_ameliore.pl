/*
specialite(etudiant(nom,prenom),specialite)
etudiant(nom,prenom)

specialite(X,Y).
->
X = etudiant('MacKensie','Jackson')
Y = 'CASPAR' ? ;

specialite(etudiant(X,Y),Z).
->

X = 'MacKensie'
Y = 'Jackson'
Z = 'CASPAR' ? :
*/


specialite(etudiant('MacKensie','Jackson'),'CASPAR').
specialite(etudiant('Cameran','Acevedo'),'IAM').
specialite(etudiant('Amela','Crane'),'IAM').


etudiant('Merritt','Ramsey','WEB').
etudiant('Dale','Cannon','IAM').
etudiant('Alexander','Ferguson','CASPAR').
etudiant('Serina','Cunningham','AL').
etudiant('Britanni','Beard','WEB').
etudiant('Sopoline','Alvarado','CASPAR').
etudiant('Madeson','Cook','AL').
etudiant('Kellie','Harrell','WEB').
etudiant('Quamar','Michael','IAM').
etudiant('Ria','Figueroa','IAM').
etudiant('September','Espinoza','WEB').
etudiant('Amena','Koch','IHM').
etudiant('Herrod','Reynolds','IHM').
etudiant('Rahim','Daniel','WEB').
etudiant('Chelsea','Merrill','WEB').
etudiant('Shaine','Marshall','IAM').
etudiant('Samson','Morrow','IAM').
etudiant('Camden','Long','CASPAR').
etudiant('Brynn','Mccray','AL').
etudiant('Julie','Stark','WEB').
etudiant('Gloria','White','WEB').
etudiant('Keiko','Castaneda','IAM').
etudiant('Howard','Mercado','AL').
etudiant('Shelby','Benson','AL').
etudiant('Angela','York','IHM').
etudiant('Constance','Orr','CASPAR').
etudiant('Derek','Salinas','AL').
etudiant('Sylvia','Velez','WEB').
etudiant('Lesley','Slater','IHM').
etudiant('Kirsten','George','WEB').
etudiant('Taylor','Mccoy','IAM').
etudiant('Rhona','Whitaker','WEB').
etudiant('Yoko','Holman','AL').
etudiant('Jordan','Bowers','CASPAR').
etudiant('Colette','Guy','IHM').
etudiant('Rebekah','Hinton','IHM').
etudiant('Walker','Knowles','IAM').
etudiant('Martena','Mckinney','CASPAR').
etudiant('Kylee','Alston','CASPAR').
etudiant('Nelle','Mcclain','AL').
etudiant('Destiny','Potter','AL').
etudiant('Owen','French','WEB').
etudiant('Angelica','Bender','IAM').
etudiant('Kyla','Warren','IAM').
etudiant('Vaughan','Mercado','IHM').
etudiant('Melissa','Strickland','IAM').
etudiant('Maggy','Cohen','CASPAR').
etudiant('Latifah','Bradshaw','CASPAR').
etudiant('Lara','Velasquez','IHM').
etudiant('Janna','Gonzales','AL').
etudiant('Harding','Burns','CASPAR').
etudiant('Stella','Gill','WEB').
etudiant('Winter','Wilder','AL').
etudiant('Illana','Benjamin','IHM').
etudiant('Joseph','Mckee','CASPAR').
etudiant('Zorita','Horton','IHM').
etudiant('Steven','Hall','IAM').
etudiant('Evelyn','Edwards','AL').
etudiant('Rhiannon','Mueller','IHM').
etudiant('Hiroko','David','IAM').
etudiant('Brenda','Clarke','AL').
etudiant('Bevis','Hopper','AL').
etudiant('Kiara','Tanner','WEB').
etudiant('Jenna','Taylor','WEB').
etudiant('Oren','Delacruz','IAM').
etudiant('Cade','Barrera','CASPAR').
etudiant('Laura','Montgomery','AL').
etudiant('Kirestin','Lucas','WEB').
etudiant('Ferris','Hickman','IAM').
etudiant('Erin','Wise','IAM').
etudiant('Grant','Golden','AL').
etudiant('Gwendolyn','Lambert','IHM').
etudiant('Scarlet','Hinton','IAM').
etudiant('Kellie','Weber','AL').
etudiant('Lester','Roy','AL').
etudiant('Cameran','Diaz','AL').
etudiant('Fatima','Rios','IHM').
etudiant('Hannah','Baxter','IHM').
etudiant('Julian','Gilmore','AL').
etudiant('Cooper','Perez','CASPAR').
etudiant('Maite','Spencer','CASPAR').
etudiant('Shaine','Galloway','CASPAR').
etudiant('Philip','Shepard','CASPAR').
etudiant('Faith','Ferrell','AL').
etudiant('Amena','Lopez','IAM').
etudiant('Keefe','Wade','WEB').
etudiant('Daquan','Foley','WEB').
etudiant('Dalton','Perez','AL').
etudiant('Simone','Hicks','IHM').
etudiant('Perry','Schneider','IHM').
etudiant('Isadora','Farrell','IHM').
etudiant('Iliana','Tucker','IAM').
etudiant('Debra','Mckenzie','IAM').
etudiant('Ian','Spence','IAM').
etudiant('Kaden','Holder','IHM').
etudiant('Nevada','Bentley','AL').
etudiant('Fletcher','Harrell','IAM').