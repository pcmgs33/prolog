creneau(1,lun,am,'algorithmic_approach').
creneau(1,mar,pm,'conception_evaluation_ihm').

/*
creneau(2,mer,pm,'conceptionsdftion_ihm').
parcours('IHM',conception_evaluation_ihm).

*/

parcours('IHM',conception_evaluation_ihm).
etudiant('Audric','Chabert','IHM').

add(X, L, [X|L]).
add_list([], L, L).
add_list([H|T], L, L1) :- add(H, L2, L1), add_list(T, L, L2).
/*
liste_option_possible('Audric','Chabert',L).
*/

liste_option_possible(Nom,Prenom,L) :-
	findall(X,creneau(_,_,_,X),COURS),
	etudiant(Nom,Prenom,Filiere),
	setof(X,parcours(Filiere,X), OBLIG),
	itere_debug(OBLIG,COURS,L).

itere_debug(_,[],[]).
itere_debug(OBLIG,[H_COURS|T_COURS],[H_COURS|RES]) :-
	itere_debug(OBLIG,T_COURS,RES),
	isNotMember(H_COURS,OBLIG),
	verifChevauchement([H_COURS|OBLIG],[]),
	display(RES),
	display(H_COURS),
	append(H_COURS,RES,L),
	display(L).

itere_debug(OBLIG,[H_COURS|T_COURS],L) :-
	itere_debug(OBLIG,T_COURS,L).



isNotMember(L1,L2) :-
	member(L1,L2),!,fail;
	true.

verifChevauchement([],_).

verifChevauchement([H|T],V) :-
	append(T,V,TOT),
    verifSimple(H,TOT),
    verifChevauchement(T,[H|V]).

/*
verifSimple
    verifie qu'un cours ne chevauche pas avec un des cours de la liste
*/
verifSimple(_,[]).

verifSimple(X,[H|T]) :-
    verifTresSimple(X,H),!,
    verifSimple(X,T).
    
/*
 verifTresSimple
 verifie que deux cours ne se chevauchent pas
 
pour ce faire il vérifie :
 que les cours ne soient pas sur la meme période 				( si c'est le cas alors il n'y a pas de chevauchement du tout)
 OU BIEN que les cours ne soient pas sur le meme jour 			(idem)
 OU BIEN que les cours ne soient pas sur la même demi-journée	(idem)

 */
verifTresSimple(X,Y) :-
	verifPeriode(X,Y);
	verifJour(X,Y);
	verifDemijour(X,Y).

/*
	On récupère ici la liste des périodes, qui peut donc être [1], [2] ou [1,2]
	on vérifie ensuite que les périodes soient bien distinctes
*/
verifPeriode(X,Y) :-
    setof(P,creneau(P,_,_,X),P1),
    setof(P,creneau(P,_,_,Y),P2),
    memberPeriode(P1,P2).

/*
	On récupère les jours des deux matières, si elles sont différentes alors les cours ne se chevauchent pas
*/
verifJour(X,Y) :-
    creneau(_,J1,_,X),
    creneau(_,J2,_,Y),
    J1 \= J2.

/*
	On récupère les demi-journées des deux matières, si elles sont différentes alors les cours ne se chevauchent pas
*/
verifDemijour(X,Y) :-
    creneau(_,_,DJ1,X),
    creneau(_,_,DJ2,Y),
    DJ1 \= DJ2.

/*
	Vérifie que les périodes des deux cours soient bien distinctes
	Si les cours sont sur au moins une période en commun, alors le test échoue
	sinon les cours sont bien sur deux périodes distinctes
*/
memberPeriode([],[]).
memberPeriode(P1,P2) :-
	member(1,P1),member(1,P2),!,fail;
	member(2,P1),member(2,P2),!,fail;
	true.