/*
le cours requis est placé avant le cours concerné


.......................................
TODO
faire les cas sans et avec listes
..........................................

requis(1,cours,cours).
requis(2,cours,cours).
moins bien car
- redondance "inutile"

requis([1,2],cours,cours).
mieux! car
- plus lisible
- 

AU FINAL cette derniere forme est peu utile car
le cas ou un cours est requis sur deux périodes n'apparaît pas ici
D'ailleurs il faut mettre deux fois les périodes, car les cours ont chacun leur période
et un cours sur une période(ou deux) peut avoir un cours pré/co-requis sur une période
(ou deux)



forme:
prerequis()
corequis()

conjointement()
a ne pas utiliser car trop lourde, pénible et source d'erreur
a l'avantage de simplifier le cas programmation_webCLIENT/SERVER

trouver si possible un script qui échange le dernier mot pour le mettre devant


Note :
la disposition prise pour requis(1,2,3,4)
fait que x1 >= x2
en effet on ne peut pas dire qu'un cours en période 1 doit etre pris avant de faire
le cours requis en periode 2

expliquer que le premier est le cours dépendant
donc le cours dépendant ne peut dépendre que de quelqun avant ou au meme moment que lui
, et pas après

*/

requis(1,1,systemes_applications_embarques,objets_connectes_services).

requis(2,1,web_donnees,web_semantique).
requis(2,1,cryptographie_securite,preuve_cryptographie).
requis(2,1,cryptographie_securite,securite_reseaux).

requis(2,2,programmation_web_server,programmmable_web_client).
requis(2,2,programmmable_web_client,programmation_web_server).

requis(2,1,conception_evaluation_ihm,interface_tactile).
requis(2,1,conception_evaluation_ihm,interface_multisupport).



