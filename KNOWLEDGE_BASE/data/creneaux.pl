creneau(1,lun,am,algorithmic_approach).
creneau(1,lun,am,conception_evaluation_ihm).
creneau(1,lun,am,techno_donnees_massives).

creneau(1,lun,pm,compression).
creneau(1,lun,pm,cryptographie_securite).
creneau(1,lun,pm,soa_integration_services).

creneau(1,mar,am,formal_models_computation).
creneau(1,mar,am,peer_to_peer).
creneau(1,mar,am,plateformes_logicielle_mobile).
creneau(1,mar,am,web_donnees).

creneau(1,mar,pm,administration_reseau).
creneau(1,mar,pm,ingenierie_connaissances).
creneau(1,mar,pm,objets_connectes_services).
creneau(1,mar,pm,performance_eval_networks).
creneau(1,mar,pm,genericite_meta_programmation).




creneau(1,mer,am,application_relationnelles_web).
creneau(1,mer,am,SOA_process_metier).
creneau(1,mer,am,conception_application_multimedia).
creneau(1,mer,am,content_distribution_wireless_networks).
creneau(1,mer,am,securite_applications_web).
creneau(1,mer,am,systemes_applications_embarques).

creneau(1,mer,pm,technique_expression).

creneau(1,jeu,am,technique_interaction_multimodalite).
creneau(1,jeu,am,traitement_avance_images).

creneau(1,jeu,pm,anglais).
creneau(1,jeu,pm,management).

creneau(1,ven,am,adaptation_interface_environnement).
creneau(1,ven,am,architectures_logicielles).
creneau(1,ven,am,evolving_internet).

creneau(1,ven,pm,applications_multimedia).
creneau(1,ven,pm,cybersecurite).
creneau(1,ven,pm,architecture_logicielle_cloud).




creneau(2,lun,am,conception_evaluation_ihm).
creneau(2,lun,am,virtualized_infrastructure_cloud).

creneau(2,lun,pm,interpretation_langages).
creneau(2,lun,pm,ingenierie_3D).
creneau(2,lun,pm,programmable_web_server).
creneau(2,lun,pm,securite_reseaux).
creneau(2,lun,pm,algorithm_telecomm).

creneau(2,mar,am,fouille_donnees).
creneau(2,mar,am,middleware_IOT).
creneau(2,mar,am,preuve_cryptographie).
creneau(2,mar,am,realite_virtuelle).
creneau(2,mar,am,retro_ingenierie_maintenance_evolution_logiciels).

creneau(2,mar,pm,large_scale_systems).
creneau(2,mar,pm,objets_connectes_services).
creneau(2,mar,pm,web_semantique).

creneau(2,mer,am,analyse_indexation_images).
creneau(2,mer,am,systemes_applications_embarques).
creneau(2,mer,am,langages_speciciques_domaines).

creneau(2,mer,pm,distributed_optimization).

creneau(2,jeu,am,bit_torrent_to_privacy).
creneau(2,jeu,am,programmmable_web).
creneau(2,jeu,am,smart_cards).

creneau(2,jeu,pm,green_networking).
creneau(2,jeu,pm,management2).

creneau(2,ven,am,architectures_logicielles).
creneau(2,ven,am,gestion_donnees_multimedia).
creneau(2,ven,am,interface_multisupport).
creneau(2,ven,am,internet_measurement).

creneau(2,ven,pm,interface_tactile).
creneau(2,ven,pm,security_privacy).
