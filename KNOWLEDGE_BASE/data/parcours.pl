parcours('IHM').
parcours('IAM').
parcours('CASPAR').
parcours('WEB').
parcours('AL').

parcours('IAM',plateformes_logicielle_mobile).
parcours('IAM',systemes_applications_embarques).
parcours('IAM',objets_connectes_services).
parcours('IAM',middleware_IOT).

parcours('IHM',conception_evaluation_ihm).
parcours('IHM',technique_interaction_multimodalite).
parcours('IHM',adaptation_interface_environnement).
parcours('IHM',interface_tactile).
parcours('IHM',interface_multisupport).


parcours('CASPAR',cryptographie_securite).
parcours('CASPAR',securite_applications_web).
parcours('CASPAR',cybersecurite).
parcours('CASPAR',securite_reseaux).
parcours('CASPAR',preuve_cryptographie).
parcours('CASPAR',security_privacy).

parcours('WEB',conception_application_multimedia).
parcours('WEB',analyse_indexation_images).
parcours('WEB',compression).
parcours('WEB',applications_multimedia).
parcours('WEB',traitement_avance_images).
parcours('WEB',techno_donnees_massives).

parcours('AL',soa_integration_services).
parcours('AL',soa_integration_services).
parcours('AL',architectures_logicielles).
parcours('AL',architecture_logicielle_cloud).
parcours('AL',retro_ingenierie_maintenance_evolution_logiciels).
parcours('AL',langages_speciciques_domaines).

/*TODO 
ne pas prendre en compte dans parcours les periodes
	pas besoin?

verifier les duplications de matières
revoir les noms des matieres
	done
*/

/*
getParcours(P,L).
	renvoit dans L la liste de tous les cours (différents, ici la période n'est pas prise en compte, ou pas)
	du parcours P



WORK: ..................

.....................................................................
essai 1 :
 ne marche pas a cause de member

 not_member(E,L) :- member(E,L),!,fail ; true.

getAllParcours(P,L) :- parcours(P,E), not_member(E,L), getAllParcours(P,[E|L]).

.....................................................................
essai 2 :
utilisationde setof
voir ce lien : 
http://stackoverflow.com/questions/1468150/how-do-i-find-all-solutions-to-a-goal-in-prolog
réponse principale, beaucoup de liens

getParcours renvoit tous les cours L du parcours P

.....................................................................
essai 3 : getAllParcours à partir de essai 2

getAllParcours(L) :- bagof(Z, parcours(_,Z), L).

ne fonctionne pas

Note : on a remplacé bagof par setof pour éviter les duplicats 
(même s'ils ne doivent pas arriver)

http://stackoverflow.com/questions/1468150/how-do-i-find-all-solutions-to-a-goal-in-prolog
http://www.swi-prolog.org/pldoc/doc_for?object=setof/3
	(voir la hierarchie des fonctions)
*/

getParcours(P,L) :- setof(Z, parcours(P,Z), L).

/* 
	member
	déjà défini/non utilisé


member(E,[E|T]).
member(E,[H|T]) :- E\=H, member(E,T). 

ou bien


member(E,[E|T]).
member(E,[_|T]) :- member(E,T). 

ce qui fait not_member

not_member(E,L) :- member(E,L),!,fail ; true.
*/


/*

verifyCours(P,C) :- getParcours(P,L), 	
*/