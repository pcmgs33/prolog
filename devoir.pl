/*
 * 
 tout
    données
        etudiants
        cours
        parcours
    fonctions
    parcours
        cours obligatoires
    cours
        pas de chevauchements
    etudiants
        lier nom prenom a un etudiant
    prerequis (et co requis)
    credits
        calculer combien de crédits vaut une liste de cours
    emploi du temps
        calculer un emploi du temps
        vérifier un emploi du temps
            pas de chevauchements
*/

/*
 * objectif : "atomiser" au maximum 
 * les faits pour avoir plus de souplesse
 * pour la suite
 * 
 * on définit individuellement les cours
 * ( par leur seul nom, qui doit donc être unique)
 * idem etudiant(paire nom-prenom <-> ) et filiere
 */
cours(programmation_web).
cours(arithmetique).


etudiant(audric,chabert).

filiere(ihm).

/*
 * affecter un cours a une filiere
 * en tant que parcours
 * (alias cours obligatoire de la
 * specialisation)
 */
parcours(ihm,programmation_web).
parcours(ihm,arithmetique).

periode(1,programmation_web).
creneau(am,programmation_web).
jour(lundi,programmation_web).

periode(1,arithmetique).
creneau(am,arithmetique).
jour(mardi,arithmetique).

/*
 * probleme de cette organisation ci dessous:
 * comment avoir facilement le fait que le cours
 * est sur la periode 1 ET 2?
 * il serait plus simple de manipuler une liste
 * 
 * une autre option, moins elegante, serait
 * d'avoir une convention pour les 3 cas
 * par exemple 1 2 3
 * 
 * il serait plus simple de commencer par
 * la solution 3, la plus facile a faire
 */
periode(programmation_web,1).
periode(programmation_web,2).

/*
 * il serait beaucoup plus souple d'utiliser
 * la notation
 * prerequis(cours(bla),cours(blu)).
 * 
 * idem pour la periode
 * 
 * en fait il faudrait utiliser cours
 * pour toutes les fonctions les manipulant
 */

/*
 * note
 * le fait de tester l'existence avec 
 * isCoursExiste pour vérifier que le cours
 * existe bien
 * 
 * pour une fonction de la forme : 
 * prerequis(cours(a),cours(b)).
 * 
 */
/*
 * cette méthode comme avantages :
 * de tester l'existence d'un cours
 * de vérifier le prédicat requis
 * 
 * attention :
 *  - la notation de l'appel prerequis(cours(X),cours(Y))
 *    est lourde et peut être inutile, selon les autres fonctions 
 *  - elle reviens à peu près à la fonction requis(X,Y).
 *    qui est largement suffisante si la base est cohérente
 *    c a d ne  contient pas d'informations erronées
 * 
 * cette méthode ne prend pas en charge les cas 
 * des cours sur deux périodes
 */
 /*
  * une possiblité de développement est de partir des relations 
  * pour arriver au schéma de données
  */
prerequis(cours(X),cours(Y)) :- cours(X),cours(Y),requis(X,Y).


/* test top-down approach
 */
controleParcours(Nom,Prenom,Cours_opt) :-
    etudiant(Nom,Prenom),
    verifCours(Nom,Prenom,Cours_opt).
    
verifCours(Nom,Prenom,Cours_opt) :-
    filiere(Nom,Prenom,Filiere),
    verifCoursAvecFiliere(Filiere,Cours_opt),
    verifCredit(Cours_opt).

verifCoursAvecFiliere(Filiere,Cours_opt) :-
    getCoursFromFiliere(Filiere,Liste_cours_filiere),
    verifChevauchement(Liste_cours_filiere,Cours_opt).

/*
 * vu que les cours sont a la meme place
 * il faut verifier qu'aucun cours ne se déroule sur
 * la meme demi-journée
 */
verifChevauchement([],_).
verifChevauchement([Liste_cours_filiere_debut | Liste_cours_filiere_fin] ,Liste_cours_opt) :-
    verifSimple(Liste_cours_filiere_debut,Liste_cours_opt).
    verifChevauchement(Liste_cours_filiere_fin,Liste_cours_opt).

verifSimple(X,[H|T]).
    verifTresSimple(X,H),
    verifSimple(X,T).

/*
 verifTresSimple
 verifie que deux cours ne se chevauchent pas
 
pour ce faire il vérifie :
 que les cours ne soient pas sur la meme période ( si c'est vrai alors tout baigne)
 OU BIEN que les cours ne soient pas sur le meme créneau (idem)
 OU BIEN que ... jour

 */

verifTresSimple(X,Y) :-
    periode(X,P1),
    periode(Y,P2),
    P1 \= P2.

verifTresSimple(X,Y) :-
    creneau(X,C1),
    creneau(Y,C2),
    C1 \= C2.

verifTresSimple(X,Y) :-
    jour(X,C1),
    jour(Y,C2),
    C1 \= C2.

/*
 * le fait d' "atomiser "
 * cela a comme inconvénients de rendre difficile la mise à jour cohérente des donnes
 * car elle est répartie sur plusieurs prédicats
 * 
 * donc faire une liste de tout ce que l'on doit définir sur tel ou tel chose
 */