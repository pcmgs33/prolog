
cours('prolog', ['prolog', 'algo'], 8).
cours('algo', ['algo'], 4).
cours('web', ['web'], 4).

majeures('ihm', ['ihm', 'sgbd', 'C++', 'java'], 12).
majeures('sgbd', ['sgbd', 'noSQL', 'C', 'java'], 12).

%
%
%

not_member(X,L) :- member(X,L), !, fail
;
true.

recursion([], _).
recursion([H|T], Ps) :- not_member(H, Ps), recursion(T, Ps).

mineures([], 0).
mineures(P, L) :- cours(_, X, Ls), Ln is L - Ls, L > 0, mineures(Ps, Ln), recursion(X, Ps), append(X, Ps, Pt), flatten(Pt, P).

programme(N, P) :- majeures(N,Pa, 12), mineures(Pn, 12), append(Pa, Pn, P).
