add(0,Y,Y).
add(succ(X),Y,succ(Z)) :- add(X,Y,Z).

min([H|T],Y):- min(T,H,Y).

min([],Min,Min).
min([H|T],Min,Y):-
	M1 is min(H,Min),
	min(T,M1,Y).